package configurations;

import org.testng.annotations.DataProvider;

public class DataProviderClass {

    @DataProvider(name = "test-data")
    public static Object[][] dataProviderMethodWithTestData(){
        return new Object[][] {{ "ТЕСТ", "12345" }, { "Авто Тест", "12345678"} };
    }
}