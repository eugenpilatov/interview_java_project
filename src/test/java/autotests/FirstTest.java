package autotests;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class FirstTest {

    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("autotests.FirstTest class started");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("All tests in autotests.FirstTest finished");
    }

    @BeforeMethod
    public void beforeTest() {
        System.out.println("Test start");
    }

    @AfterMethod
    public void afterTest() {
        System.out.println("Test finished");
    }

    @Test
    public void myTest() {
        Assert.assertEquals(1 / 0, 1);
    }

    @Test
    public void test1() {
        Assert.assertEquals(4, 2 + 2);
    }

    @Test
    public void test2() {
        Assert.assertEquals(5, 2 + 2);
    }

    @Test
    public void test3() {
        Assert.assertTrue(2 + 2 == 4);
    }

    @Test
    public void test4() {
        Assert.assertTrue(2 + 2 == 5);
    }

    @Test
    public void test5() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(2 + 2, 4);
        softAssert.assertEquals(2 + 2, 5);
        softAssert.assertTrue(2 + 2 == 4, "check 2+2 = 4");
        softAssert.assertTrue(2 + 2 == 5, "check 2+2 = 5");
        softAssert.assertAll();
    }
}

